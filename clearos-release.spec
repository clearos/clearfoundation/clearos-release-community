%define debug_package %{nil}
%define product_vendor clear
%define software_id 10690
%define product_family ClearOS
%define variant_titlecase Community
%define variant_lowercase community
%define release_name Final
%define base_release_version 6
%define full_release_version 6.10.0

#% define beta Beta

Name:           clearos-release%{?variant_lowercase:-%{variant_lowercase}}
Version:        6
Release:        6.10.0.1%{?dist}
Summary:        %{product_family}%{?variant_titlecase: %{variant_titlecase}} release file
Group:          System Environment/Base
License:        GPLv2
BuildArch:      noarch
Obsoletes:      redhat-release-server rawhide-release comps rpmdb-redhat fedora-release
Obsoletes:      redhat-release-as redhat-release-es redhat-release-ws redhat-release-de
Obsoletes:      centos-release
Obsoletes:      clearos-release
Provides:       clearos-release redhat-release system-release redhat-release-server
Provides:       clearos-release-jws = 1.1
Provides:       community-release
Source0:        clearos-release-6-3.tar.gz
Source1:        clearos.repo
Source2:        clearos-centos.repo
Source3:        centos-unverified.repo

%description
%{product_family}%{?variant_titlecase: %{variant_titlecase}} release files

%prep
%setup -q -n clearos-release-%{base_release_version}

%build
echo OK

%install
rm -rf $RPM_BUILD_ROOT

# create /etc
mkdir -p $RPM_BUILD_ROOT/etc

# create /etc/clearos-release, /etc/system-release and /etc/redhat-release
echo "%{product_family}%{?variant_titlecase: %{variant_titlecase}} release %{full_release_version}%{?beta: %{beta}} (%{release_name})" > $RPM_BUILD_ROOT/etc/clearos-release
ln -s clearos-release $RPM_BUILD_ROOT/etc/redhat-release
ln -s clearos-release $RPM_BUILD_ROOT/etc/system-release

# create /etc/product
cat > $RPM_BUILD_ROOT/etc/product << EOF
# Product details
vendor = %{product_vendor}
software_id = %{software_id}
name = %{product_family}%{?variant_titlecase: %{variant_titlecase}}
version = %{full_release_version}
base_version = %{base_release_version}

# Registration
free_trial = 1
portal_url = https://secure.clearcenter.com/portal

# Links
redirect_url = http://www.clearcenter.com/redirect

# Java Web Services
jws_nodes = 1
jws_prefix = cos6-ws
jws_domain = clearsdn.com
jws_realm = ws
jws_version = 1.1
EOF

# write cpe to /etc/system/release-cpe
echo "cpe:/o:redhat:enterprise_linux:%{version}:%{?beta:beta}%{!?beta:GA}%{?variant_lowercase::%{variant_lowercase}}" | tr [A-Z] [a-z] > $RPM_BUILD_ROOT/etc/system-release-cpe

# create /etc/issue and /etc/issue.net
cp $RPM_BUILD_ROOT/etc/clearos-release $RPM_BUILD_ROOT/etc/issue
echo "Kernel \r on an \m" >> $RPM_BUILD_ROOT/etc/issue
cp $RPM_BUILD_ROOT/etc/issue $RPM_BUILD_ROOT/etc/issue.net
echo >> $RPM_BUILD_ROOT/etc/issue

# Yum repos
mkdir -p $RPM_BUILD_ROOT/etc/yum.repos.d
install -m 644 %{SOURCE1} $RPM_BUILD_ROOT/etc/yum.repos.d
install -m 644 %{SOURCE2} $RPM_BUILD_ROOT/etc/yum.repos.d
install -m 644 %{SOURCE3} $RPM_BUILD_ROOT/etc/yum.repos.d

# copy GPG keys
mkdir -p -m 755 $RPM_BUILD_ROOT/etc/pki/rpm-gpg
for file in RPM-GPG-KEY* ; do
    install -m 644 $file $RPM_BUILD_ROOT/etc/pki/rpm-gpg
done

# set up the dist tag macros
install -d -m 755 $RPM_BUILD_ROOT/etc/rpm
cat >> $RPM_BUILD_ROOT/etc/rpm/macros.dist << EOF
# dist macros.

%%rhel %{base_release_version}
%%dist .v%{base_release_version}
%%el%{base_release_version} 1
EOF

%pre
# Migrate ClearOS Core to CentOS
# Note: the various command line utils are only needed for upgrades
# and may not yet be installed on PXE/anaconda intalls.  There's no
# need to make these hard dependencies
if ( [ -x /bin/grep ] && [ -x /bin/touch ] ); then
    CHECK=$(/bin/grep "^enabled=1" /etc/yum.repos.d/clearos-core.repo 2>/dev/null)
    if [ -n "$CHECK" ]; then
        /bin/touch /etc/yum.repos.d/enablecentos
    fi
fi

exit 0

%post
CHECK=$(grep ^distroverpkg= /etc/yum.conf 2>/dev/null)
if ( [ -x /bin/grep ] &&  [ -n "$CHECK" ] ); then
    sed -i -e '/^distroverpkg=.*/d' /etc/yum.conf
fi

CHECK=$(grep ^bugtracker_url= /etc/yum.conf 2>/dev/null)
if ( [ -x /bin/grep ] &&  [ -n "$CHECK" ] ); then
    sed -i -e '/^bugtracker_url=.*/d' /etc/yum.conf
fi

if ( [ -x /bin/rm ] && [ -e /etc/yum.repos.d/enablecentos ] ); then
    /bin/rm /etc/yum.repos.d/enablecentos
    if [ -x /usr/bin/yum-config-manager ]; then
         /usr/bin/yum-config-manager --enable clearos-centos clearos-centos-updates >/dev/null 2>&1
    fi
fi

exit 0

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc GPL
%attr(0644,root,root) /etc/clearos-release
/etc/redhat-release
/etc/system-release
/etc/product
%config %attr(0644,root,root) /etc/system-release-cpe
%config(noreplace) %attr(0644,root,root) /etc/issue
%config(noreplace) %attr(0644,root,root) /etc/issue.net
%dir /etc/pki/rpm-gpg
/etc/pki/rpm-gpg/*
/etc/rpm/macros.dist
/etc/yum.repos.d/clearos.repo
%config(noreplace) /etc/yum.repos.d/clearos-centos.repo
%config(noreplace) /etc/yum.repos.d/centos-unverified.repo

%changelog
* Fri Aug 17 2018 ClearFoundation <developer@clearfoundation.com> - 6-6.10.0.1
- 6.10.0 Final

* Mon Apr 10 2017 ClearFoundation <developer@clearfoundation.com> - 6-6.9.0.1
- 6.9.0 Final

* Tue Oct 04 2016 ClearFoundation <developer@clearfoundation.com> - 6-6.8.0.3
- 6.8.0 Final with SDN style repo management

* Mon May 30 2016 ClearFoundation <developer@clearfoundation.com> - 6-6.8.0.2
- 6.8.0 Final

* Mon May 30 2016 ClearFoundation <developer@clearfoundation.com> - 6-6.8.0.1
- 6.8.0 Beta 1

* Thu Feb 17 2016 ClearFoundation <developer@clearfoundation.com> - 6-6.7.0.8
- 6.7.0 Final with bug tracker URL fix

* Thu Feb 17 2016 ClearFoundation <developer@clearfoundation.com> - 6-6.7.0.7
- 6.7.0 Final with CentOS repo updates

* Thu Aug 27 2015 ClearFoundation <developer@clearfoundation.com> - 6-6.7.0.6
- 6.7.0 Final

* Fri Aug 14 2015 ClearFoundation <developer@clearfoundation.com> - 6-6.7.0.5
- 6.7.0 Beta 1 with ad-hoc coding standard applied

* Fri Aug 14 2015 ClearFoundation <developer@clearfoundation.com> - 6-6.7.0.4
- 6.7.0 Beta 1 with PXE/anaconda changes

* Fri Aug 14 2015 ClearFoundation <developer@clearfoundation.com> - 6-6.7.0.3
- 6.7.0 Beta 1 with CentOS repo change

* Thu Aug 13 2015 ClearFoundation <developer@clearfoundation.com> - 6-6.7.0.2
- 6.7.0 Beta 1 fixed

* Wed Aug 12 2015 ClearFoundation <developer@clearfoundation.com> - 6-6.7.0.1
- 6.7.0 Beta 1

* Thu Jan 08 2015 ClearFoundation <developer@clearfoundation.com> - 6-6.6.0.3
- 6.6.0 Final release

* Mon Nov 10 2014 ClearFoundation <developer@clearfoundation.com> - 6-6.6.0.2
- Initial build for 6.6.0 Beta 2

* Wed Apr 30 2014 ClearFoundation <developer@clearfoundation.com> - 6-6.6.0.1
- Initial build for 6.6.0 Beta 1

* Tue Dec 17 2013 ClearFoundation <developer@clearfoundation.com> - 6-6.5.0.4
- Initial build for 6.5.0 Final

* Tue Nov 26 2013 ClearFoundation <developer@clearfoundation.com> - 6-6.5.0.3
- Initial build for 6.5.0 Beta 3

* Tue Oct 08 2013 ClearFoundation <developer@clearfoundation.com> - 6-6.5.0.2
- Initial build for 6.5.0 Beta 2

* Tue Aug 27 2013 ClearFoundation <developer@clearfoundation.com> - 6-6.5.0.1
- Initial build for 6.5.0 Beta 1

* Wed Mar 27 2013 ClearFoundation <developer@clearfoundation.com> - 6-6.4.0.4
- 6.4.0 Final
- Moved ClearOS Core repo to separate yum configlet [tracker #1005]

* Fri Feb 22 2013 ClearFoundation <developer@clearfoundation.com> - 6-6.4.0.3
- Initial build for 6.4.0 Beta 2

* Thu Dec 20 2012 ClearFoundation <developer@clearfoundation.com> - 6-6.4.0.2
- Initial build for 6.4.0 Beta 1

* Thu Dec 06 2012 ClearFoundation <developer@clearfoundation.com> - 6-6.4.0.1
- Initial build for 6.4.0 Alpha 1

* Tue Jul 17 2012 ClearFoundation <developer@clearfoundation.com> - 6-6.3.0.4
- Update software ID due to policy change

* Mon Jul 02 2012 ClearFoundation <developer@clearfoundation.com> - 6-6.3.0.3
- Set for final release
- Updated clearos-core exclude list

* Fri Jun 22 2012 ClearFoundation <developer@clearfoundation.com> - 6-6.3.0.2
- Disable updates-testing, Marketplace will handle it

* Fri Jun 22 2012 ClearFoundation <developer@clearfoundation.com> - 6-6.3.0.1
- Enable updates-testing for Marketplace

* Wed Jun 20 2012 ClearFoundation <developer@clearfoundation.com> - 6-6.3.0.0
- Initial build for 6.3.0 Beta 1
- Add obsoletes for CentOS to provide clean upgrade/bootstrap
- Add web service release tracking: clearos-release-jws

* Fri Jun 15 2012 ClearFoundation <developer@clearfoundation.com> - 6-6.2.1.0
- Initial build for 6.2.1 Beta 1

* Thu Jun 14 2012 ClearFoundation <developer@clearfoundation.com> - 6-6.2.0.10
- Add xulrunner to yum exclude list

* Wed May 30 2012 ClearFoundation <developer@clearfoundation.com> - 6-6.2.0.9
- Add yum exclude list with all the modified Core packages

* Fri Apr 20 2012 ClearFoundation <developer@clearfoundation.com> - 6-6.2.0.8
- Update for Final

* Tue Apr  3 2012 ClearFoundation <developer@clearfoundation.com> - 6-6.2.0.7
- Migrated to 3-digit release number
- Added mirrorlist for Core

* Thu Mar 29 2012 ClearFoundation <developer@clearfoundation.com> - 6-6.2.0.6
- Update to rc1

* Tue Mar 20 2012 ClearFoundation <developer@clearfoundation.com> - 6-6.2.0.5
- Converted yum repos to mirrorlist

* Fri Feb 24 2012 ClearFoundation <developer@clearfoundation.com> - 6-6.2.0.4
- Make package noarch

* Fri Jan 27 2012 ClearFoundation <developer@clearfoundation.com> - 6-6.2.0.3
- Split into two editions
- Update for beta3

* Thu Dec 15 2011 ClearFoundation <developer@clearfoundation.com> - 6-6.2.0.1
- Updated /etc/product for beta2

* Tue Nov 22 2011 ClearFoundation <developer@clearfoundation.com> - 6-6.1.0.4
- Update for beta2

* Fri Aug 26 2011 ClearFoundation <developer@clearfoundation.com> - 6-6.1.0.3
- Update for beta1
- Fix repo file

* Fri Jul 22 2011 ClearFoundation <developer@clearfoundation.com> - 6-6.1.0.1
- Add /etc/product

* Fri Jul 15 2011 ClearFoundation <developer@clearfoundation.com> - 6-6.1.0.0
- Initial build for ClearOS Enterprise 6.1

